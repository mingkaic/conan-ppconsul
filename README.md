# conan-ppconsul
This repo sets up conan ppconsul

# install steps
Create packages using `conan create . mingkaic-co/stable`

# usage
Before install package first add remote: `conan remote add mingkaic-co "https://api.bintray.com/conan/mingkaic-co/mkc-conan"`
Add requirement `Ppconsul/<version>@mingkaic-co/stable`
